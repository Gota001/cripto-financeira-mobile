package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.view.*;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.Button;

public class cadastro extends AppCompatActivity {
    private Button btnProximo2;
    private EditText edtDtNasc, edtCpf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        btnProximo2 = findViewById(R.id.btnProximo2);
        edtDtNasc = (EditText) findViewById(R.id.edtDtNasc);
        edtCpf = (EditText) findViewById(R.id.edtCpf);


        btnProximo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(cadastro.this, dados_pessoais.class);
                startActivity(i);
            }
        });
    }
}
