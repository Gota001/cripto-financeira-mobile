package com.example.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AdminSql extends SQLiteOpenHelper {
    public AdminSql(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name,factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table Cliente (cep integer primary key, nome text, nomeMae text, endereco text, numero text, complemento text, bairro text, cidade text, estado text, tel text, cel text)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("drop table if exists Cliente");
    db.execSQL("create table Cliente (cep integer primary key, nome text, nomeMae text, endereco text, numero text, complemento text, bairro text, cidade text, estado text, tel text, cel text)");
    }
}
