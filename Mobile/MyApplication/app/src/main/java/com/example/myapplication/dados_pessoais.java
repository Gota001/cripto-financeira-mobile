package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.view.*;

public class dados_pessoais extends AppCompatActivity {
    private EditText edtNome, edtNomeMae, edtCep, edtEndereco, edtNumero, edtComplemento, edtBairro, edtCidade, edtEstado, edtTel, edtCel;
    Button btnConcluir;
    private Cursor linha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_pessoais);

        btnConcluir = findViewById(R.id.btnConcluir);
        edtNome = (EditText) findViewById(R.id.edtNome);
        edtNomeMae = (EditText) findViewById(R.id.edtNomeMae);
        edtCep = (EditText) findViewById(R.id.edtCep);
        edtEndereco = (EditText) findViewById(R.id.edtEndereco);
        edtNumero = (EditText) findViewById(R.id.edtNumero);
        edtComplemento = (EditText) findViewById(R.id.edtComplemento);
        edtBairro = (EditText) findViewById(R.id.edtBairro);
        edtCidade = (EditText) findViewById(R.id.edtCidade);
        edtEstado = (EditText) findViewById(R.id.edtEstado);
        edtTel = (EditText) findViewById(R.id.edtTel);
        edtCel = (EditText) findViewById(R.id.edtCel);

    }

    //Função de Cadastro
    public void cadastro(View v) {
        AdminSql admin = new AdminSql(this, "Administracao", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        String nome = edtNome.getText().toString();
        String nomeMae = edtNomeMae.getText().toString();
        String cep = edtCep.getText().toString();
        String endereco = edtEndereco.getText().toString();
        String numero = edtNumero.getText().toString();
        String complemento = edtComplemento.getText().toString();
        String bairro = edtBairro.getText().toString();
        String cidade = edtCidade.getText().toString();
        String estado = edtEstado.getText().toString();
        String tel = edtTel.getText().toString();
        String cel = edtCel.getText().toString();


        Cursor linha = bd.rawQuery("select * from Cliente where cep =" + cep, null);
        if (!linha.moveToFirst()) {
            ContentValues registro = new ContentValues();

            registro.put("nome", nome);
            registro.put("nomeMae", nomeMae);
            registro.put("cep", cep);
            registro.put("endereco", endereco);
            registro.put("numero", numero);
            registro.put("complemento", complemento);
            registro.put("bairro", bairro);
            registro.put("cidade", cidade);
            registro.put("estado", estado);
            registro.put("tel", tel);
            registro.put("cel", cel);


            //Inserindo valores
            bd.insert("Cliente", null, registro);
            bd.close();

            //Limpando os campos
            edtNome.setText("");
            edtNomeMae.setText("");
            edtCep.setText("");
            edtEndereco.setText("");
            edtNumero.setText("");
            edtComplemento.setText("");
            edtBairro.setText("");
            edtCidade.setText("");
            edtEstado.setText("");
            edtTel.setText("");
            edtCel.setText("");

            Toast.makeText(this, "Dados gravados com sucesso", Toast.LENGTH_SHORT).show();
        }
        else {
            bd.close();
            Toast.makeText(this, "Cadastro já existente", Toast.LENGTH_SHORT).show();
        }
        btnConcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(dados_pessoais.this, LoginOk.class);
                startActivity(i);
            }
        });
    }
}