package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.view.*;

public class LoginErrado extends AppCompatActivity {
    private EditText edtEmail, edtSenha, edtConfirmarSenha, edtCpf, edtDtNasc;
    private Button btnProximo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_errado);

        btnProximo = findViewById(R.id.btnProximo);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtSenha = (EditText) findViewById(R.id.edtSenha);
        edtConfirmarSenha = (EditText) findViewById(R.id.edtConfirmarSenha);
        edtCpf = (EditText) findViewById(R.id.edtCpf);
        edtDtNasc = (EditText) findViewById(R.id.edtDtNasc);

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginErrado.this, dados_pessoais.class);
                startActivity(i);
            }
        });
    }
}
