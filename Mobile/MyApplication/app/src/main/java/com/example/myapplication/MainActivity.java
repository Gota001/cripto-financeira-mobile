package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.view.*;

public class MainActivity extends AppCompatActivity {

    private EditText edtBuscarEmail, edtBuscarSenha;
    private Button btnEntrar, btnRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtBuscarEmail = findViewById(R.id.edtBuscarEmail);
        edtBuscarSenha = findViewById(R.id.edtBuscarSenha);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnRegistrar = findViewById(R.id.btnRegistrar);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edtBuscarEmail.getText().toString();
                String senha = edtBuscarSenha.getText().toString();

                if (email.equals("meuinvestimento.com.br") && senha.equals("123")) {
                    Intent i = new Intent(MainActivity.this, LoginOk.class);
                    startActivity(i);
                } else {
                    Toast.makeText(MainActivity.this, "E-mail ou senha incorreto", Toast.LENGTH_SHORT).show();
                }
                //Limpando os campos
                edtBuscarEmail.setText("");
                edtBuscarSenha.setText("");
            }
        });
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, LoginErrado.class);
                startActivity(i);
            }
        });
    }
}